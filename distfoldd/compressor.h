#ifndef COMPRESSOR_H
#define COMPRESSOR_H

#include <QtCore/QByteArray>

class Compressor
{
private:
	Compressor();

public:
	static QByteArray compress(const QByteArray& data);
	static QByteArray decompress(const QByteArray& data);
};

#endif // COMPRESSOR_H
