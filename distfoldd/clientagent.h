#ifndef CLIENTAGENT_H
#define CLIENTAGENT_H

#include <QtCore/QQueue>
#include <QtNetwork/QHostAddress>

#include "agent.h"

class ClientAgent : public Agent
{
	Q_OBJECT
public:
	explicit ClientAgent(const QHostAddress& addr, uint port, const QDir& local_dir, const QString& passwd, SyncFlags flags, QObject *parent = 0);

	enum State {
		STATE_HELLO,
		STATE_AUTH,
		STATE_FILE_LIST,
		STATE_FILE_ACTIONS
	};

protected:
	void handleMessage(MessageType msg, const QByteArray &data);

private:
	void sendFileList();
	void handleActionInfoList(const RemoteActionInfoList& list);
	void executeNextAction();
	void handlePulledFile(const QByteArray& data);
	void handlePushFile(const QString& wire_path);
	void handleDeleteFile(const QString& wire_path);

private:
	State _state;
	RemoteActionInfoList _pendingActions;
	QByteArray _challenge;
};

#endif // CLIENTAGENT_H
