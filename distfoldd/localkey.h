#ifndef LOCALKEY_H
#define LOCALKEY_H

#include <QtCore/QString>

class LocalKey
{
private:
	LocalKey();

public:
	static QString localKeyDir();

	static bool setupLocalKey();
};

#endif // LOCALKEY_H
