#ifndef DISCOVERER_H
#define DISCOVERER_H

#include <QtCore/QTimer>
#include <QtCore/QUuid>
#include <QtCore/QDateTime>
#include <QtNetwork/QNetworkConfigurationManager>
#include <QtNetwork/QHostAddress>
#include <QtNetwork/QUdpSocket>
#include <QtSystemInfo/QSystemNetworkInfo>
#include <QtSystemInfo/QSystemAlignedTimer>

QTM_USE_NAMESPACE

class Discoverer : public QObject
{
	Q_OBJECT

public:
	Discoverer(const QUuid& uuid, uint port, const QString& serviceName, QObject *parent = 0);
	
	static const uint servicePort = 17451;
	static const int dateTimeCompareMinDelta = 5;

	// In seconds
	static const int idleBroadcastInterval = 15 * 60;
	static const int activeBroadcastInterval = 5;
	static const int lostHostTimeout =  idleBroadcastInterval * 1.4f;

signals:
	void foundMoreRecentHost(const QHostAddress& address, uint port, const QDateTime& dateTime);

public slots:
	void setLastModifiedTime(const QDateTime& dateTime);

private:
#pragma pack(push)
#pragma pack(1)
	struct Message {
		quint32 mtime;
		char hostUuid[38];
		quint16 port;
		char folderUuid[38];
		char name[];
	};
#pragma pack()
	struct HostInfo {
		QDateTime lastSeen;
	};

	QByteArray encodeMessage() const;

	void switchToActiveMode();

private slots:
	void broadcastMessage();
	void handleDataAvailable();
	void handleBroadcastMessage(const QHostAddress& sender, const QByteArray &data);
	void handleHostSeen(const QUuid& hostUuid);
	void handleTimerTimeout();
	void handleTimerError();
	void handleOnlineStateChanged(bool online);

private:
	QUuid _folderUuid;
	QUuid _hostUuid;
	QString _serviceName;
	uint _port;
	QDateTime _mtime;
	bool _mtimeChanged;
	QUdpSocket *_receiver;
	QUdpSocket *_sender;
	quint32 _myId;
	QNetworkConfigurationManager *_netConfig;
	QSystemNetworkInfo *_netInfo;
	QDateTime _lastBroadcast;
	QSystemAlignedTimer *_timer;
	QTimer *_fallbackTimer;
	QHash<QUuid, HostInfo> _knownHosts;
};

#endif // DISCOVERER_H
