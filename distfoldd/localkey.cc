#include <QtCore/QDir>
#include <QtCore/QDebug>
#include <QtCrypto>

#include "localkey.h"

LocalKey::LocalKey()
{
}

QString LocalKey::localKeyDir()
{
	return QDir::home().absoluteFilePath(".config/distfold");
}

bool LocalKey::setupLocalKey()
{
	QDir local_key_dir(localKeyDir());
	if (local_key_dir.exists("server.crt") && local_key_dir.exists("server.key")) {
		return true;
	}
	if (!local_key_dir.exists()) {
		if (!local_key_dir.mkpath(local_key_dir.absolutePath())) {
			qWarning() << "Could not create local key directory";
			return false;
		}
	}

	QCA::KeyGenerator keygen;
	keygen.setBlockingEnabled(true);

	qDebug() << "Generating private key...";
	QCA::PrivateKey private_key = keygen.createRSA(2048);
	if (!private_key.toPEMFile(local_key_dir.absoluteFilePath("server.key"))) {
		qWarning() << "Failed to save private key";
		return false;
	}

	qDebug() << "Generating server certificate...";
	QCA::CertificateInfo cert_info;
	cert_info.insert(QCA::CommonName, "Distfold Private Generic Cert");
	QCA::CertificateOptions cert_options;
	cert_options.setAsCA(1);
	cert_options.setInfo(cert_info);
	cert_options.setValidityPeriod(QDateTime::currentDateTime(),
	                               QDateTime::currentDateTime().addYears(2));
	QCA::Certificate cert(cert_options, private_key);
	if (!cert.toPEMFile(local_key_dir.absoluteFilePath("server.crt"))) {
		qWarning() << "Failed to save server certificate";
		return false;
	}

	return true;
}
