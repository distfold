#include <QtCore/QDebug>
#include <QtCore/QDir>
#include <QtNetwork/QSslSocket>

#include "localkey.h"
#include "server.h"

Server::Server(QObject *parent) :
    QTcpServer(parent)
{
	loadKeys();
	if (!listen()) {
		qWarning() << "Failed to start server socket";
	}
}

void Server::loadKeys()
{
	QDir local_key_dir(LocalKey::localKeyDir());
	QFile cert_file(local_key_dir.absoluteFilePath("server.crt"));
	if (cert_file.open(QIODevice::ReadOnly)) {
		_cert = QSslCertificate(&cert_file, QSsl::Pem);
		cert_file.close();
	}
	if (_cert.isNull()) {
		qWarning() << "Could not load server certificate";
	}
	QFile key_file(local_key_dir.absoluteFilePath("server.key"));
	if (key_file.open(QIODevice::ReadOnly)) {
		_key = QSslKey(&key_file, QSsl::Rsa, QSsl::Pem);
		key_file.close();
	}
	if (_key.isNull()) {
		qWarning() << "Could not load private key";
	}
}

void Server::incomingConnection(int socketDescriptor)
{
	QSslSocket *socket = new QSslSocket(this);
	connect(socket, SIGNAL(error(QAbstractSocket::SocketError)),
	        SLOT(handleSocketError(QAbstractSocket::SocketError)));
	if (socket->setSocketDescriptor(socketDescriptor)) {
		socket->setLocalCertificate(_cert);
		socket->setPrivateKey(_key);
		socket->setPeerVerifyMode(QSslSocket::QueryPeer);
		socket->startServerEncryption();
		addPendingConnection(socket);
	} else {
		delete socket;
	}
}

void Server::handleSocketError(QAbstractSocket::SocketError error)
{
	QSslSocket *socket = qobject_cast<QSslSocket*>(sender());
	qDebug() << "Server socket error:" << socket->error() << socket->errorString();
}
