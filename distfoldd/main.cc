#include <QtCore/QCoreApplication>
#include <QtCore/QSettings>
#include <QtCore/QDebug>
#include <QtCrypto>

#include "distfolder.h"
#include "localkey.h"

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);
	a.setOrganizationName("distfold");
	a.setOrganizationDomain("com.javispedro.distfold");
	a.setApplicationName("distfoldd");
	a.setApplicationVersion("0.3");

	QCA::Initializer qca;
	qsrand(uint(QDateTime::currentMSecsSinceEpoch() & 0xFFFF));

	if (!QCA::isSupported("cert,pkey,rsa,sha1,hmac(sha1),random")) {
		qWarning() << "QCA was not build with the OpenSSL plugin";
		return EXIT_FAILURE;
	}

	if (!LocalKey::setupLocalKey()) {
		qWarning() << "Failed to setup local private key";
		return EXIT_FAILURE;
	}

	QSettings settings;
	foreach (const QString& group, settings.childGroups()) {
		settings.beginGroup(group);
		QUuid uuid(settings.value("uuid").toString());
		QString path = settings.value("path").toString();
		DistFolder *f = new DistFolder(uuid, path);
		f->setPassword(settings.value("password", uuid.toString()).toString());
		f->setReadOnlySync(settings.value("ro", false).toBool());
		f->setPullMode(settings.value("pull", false).toBool());
		f->setCompress(settings.value("compress", true).toBool());
		settings.endGroup();
	}

	return a.exec();
}
