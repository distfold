#include <QtCore/QDebug>

#include "compressor.h"

Compressor::Compressor()
{
}

QByteArray Compressor::compress(const QByteArray& data)
{
	return qCompress(data);
}

QByteArray Compressor::decompress(const QByteArray& data)
{
	return qUncompress(data);
}
