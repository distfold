TARGET   = distfoldd
TEMPLATE = app
CONFIG   += console
CONFIG   -= app_bundle

QT       += core network
QT       -= gui

CONFIG   += mobility
MOBILITY += systeminfo

CONFIG   += crypto

SOURCES += main.cc \
    distfolder.cc \
    server.cc \
    watcher.cc \
    clientagent.cc \
    serveragent.cc \
    agent.cc \
    discoverer.cc \
    compressor.cc \
    localkey.cc

HEADERS += \
    distfolder.h \
    server.h \
    watcher.h \
    clientagent.h \
    serveragent.h \
    agent.h \
    discoverer.h \
    compressor.h \
    localkey.h

contains(MEEGO_EDITION,harmattan) {
	target.path = /opt/distfold/bin
	INSTALLS += target

	service.files = distfoldd.conf
	service.path = /etc/init/apps
	INSTALLS += service
}
