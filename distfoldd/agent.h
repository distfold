#ifndef AGENT_H
#define AGENT_H

#include <QtCore/QDateTime>
#include <QtCore/QDir>
#include <QtCore/QCryptographicHash>
#include <QtNetwork/QSslSocket>

#include "compressor.h"

class Agent : public QObject
{
	Q_OBJECT
	Q_FLAGS(SyncFlag SyncFlags)

public:
	enum SyncFlag {
		SYNC_NORMAL = 0,
		SYNC_PULL = 0x1,
		SYNC_READ_ONLY = 0x2,
		SYNC_COMPRESS = 0x8
	};
	Q_DECLARE_FLAGS(SyncFlags, SyncFlag)

public:
	explicit Agent(QSslSocket *socket, const QDir& dir, const QString& passwd, SyncFlags flags, QObject *parent = 0);

	static const uint servicePort = 17451;

signals:
	void finished();

protected:
#pragma pack(push)
#pragma pack(1)
	enum MessageType {
		MSG_HELLO = 0, //args: uint32 (proto_version) + string (client challenge)
		MSG_HELLO_REPLY, //args: string (server challenge)
		MSG_AUTH, //args: string (challenge response)
		MSG_AUTH_REPLY, // args: uint8 (AuthResult)
		MSG_SET_SUBROOT, //args: string
		MSG_FILE_LIST, //args: FileListItem[]
		MSG_FILE_ACTIONS_REPLY, //args: ActionItem[]
		MSG_PULL_FILE, //args: string
		MSG_PULL_FILE_REPLY, //args: file data
		MSG_PUSH_FILE, //args: FileNameItem + file data
		MSG_PUSH_FILE_METADATA, //args: FileListItem[]
		MSG_DELETE_FILE, //args: string
		MSG_BYE
	};
	struct MessageHeader {
		quint32 msg;
		quint32 len;
	};
	enum ProtoVersion {
		PROTO_BAD = -1,
		PROTO_0 = 0,
		PROTO_1 = 1,
		PROTO_CURRENT = PROTO_1
	};
	enum AuthResult {
		AUTH_OK = 0,
		AUTH_FAILED
	};
	enum FileType {
		FILE_TYPE_NONE = 0,
		FILE_TYPE_FILE,
		FILE_TYPE_DIR
	};
	struct FileListItem {
		quint64 size;
		quint32 mtime;
		quint16 name_len;
		quint8 type;
		char name[];
	};
	enum FileAction {
		ACTION_NONE = 0,
		ACTION_PULL_METADATA,
		ACTION_PUSH_METADATA,
		ACTION_PULL,
		ACTION_PUSH,
		ACTION_PULL_DELETE,
		ACTION_PUSH_DELETE
	};
	struct ActionItem {
		quint64 size;
		quint32 mtime;
		quint16 name_len;
		quint8  type;
		quint8  action;
		char name[];
	};
	struct FileNameItem {
		quint16 name_len;
		char name[];
	};
#pragma pack(pop)

protected:
	class RemoteFileInfo {
	public:
		RemoteFileInfo();
		RemoteFileInfo(const QString &name, FileType type, const QDateTime &mtime, qint64 size);

		inline qint64 size() const {
			return _size;
		}

		inline QDateTime lastModified() const {
			return _mtime;
		}

		inline QString name() const {
			return _name;
		}

		inline bool isDir() const {
			return _type == FILE_TYPE_DIR;
		}

	private:
		QString _name;
		FileType _type;
		QDateTime _mtime;
		qint64 _size;
	};
	typedef QList<RemoteFileInfo> RemoteFileInfoList;

	class ActionInfo {
	public:
		ActionInfo(FileAction action, const QFileInfo& fileInfo);

		inline FileAction action() const {
			return _action;
		}

		inline QFileInfo fileInfo() const {
			return _info;
		}

	private:
		FileAction _action;
		QFileInfo _info;
	};
	typedef QList<ActionInfo> ActionInfoList;

	class RemoteActionInfo {
	public:
		RemoteActionInfo(FileAction action, const QString& name, FileType type, const QDateTime& mtime, qint64 size);

		inline FileAction action() const {
			return _action;
		}

		inline RemoteFileInfo fileInfo() const {
			return _info;
		}

	private:
		FileAction _action;
		RemoteFileInfo _info;
	};
	typedef QList<RemoteActionInfo> RemoteActionInfoList;

protected:
	void sendMessage(MessageType msg, const QByteArray& data = QByteArray());
	virtual void handleMessage(MessageType msg, const QByteArray& data) = 0;

	int inBufRequiredData() const;

	bool equalDateTime(const QDateTime& dt1, const QDateTime& dt2);
	void setLocalFileDateTime(const QString &path, const QDateTime& dt);

	QString wireToLocalPath(const QString& path);
	QString localToWirePath(const QString& path);

	QString wireParentPath(const QString& path);
	QString findExistingCommonAncestor(const QString& path,
	                                   const QHash<QString, QFileInfo>& local_files,
	                                   const QHash<QString, RemoteFileInfo>& remote_files);
	static bool lessPathDepthThan(const QString& s1, const QString& s2);
	static bool morePathDepthThan(const QString& s1, const QString& s2);

	QByteArray encodeHelloMessage(const QByteArray& client_challenge);
	ProtoVersion decodeHelloMessage(const QByteArray& ba, QByteArray* client_challenge);

	static QByteArray hmacSha1(const QByteArray& key, const QByteArray& message);
	QByteArray generateChallenge();
	QByteArray generateChallengeResponse(const QByteArray& server_challenge, const QByteArray& client_challenge, const QSslCertificate& server_cert, const QSslCertificate& client_cert);

	QByteArray encodeAuthReply(AuthResult result);
	AuthResult decodeAuthReply(const QByteArray& ba);

	static QFileInfoList scanFiles(const QDir& dir);
	QByteArray encodeFileInfoList(const QFileInfoList& list);
	RemoteFileInfoList decodeFileInfoList(const QByteArray& ba);

	QByteArray encodeActionInfoList(const ActionInfoList& list);
	RemoteActionInfoList decodeActionInfoList(const QByteArray& ba);
	static bool lessPathDepthThan(const ActionInfo& a1, const ActionInfo& a2);
	static bool morePathDepthThan(const ActionInfo& a1, const ActionInfo& a2);

	QByteArray encodeFileName(const QString& wire_path);
	QString decodeFileName(const QByteArray& ba);

	QByteArray encodeFileNameItem(const QString& wire_path);
	QString decodeFileNameItem(const QByteArray& ba, int* length);

protected:
	QDir _dir;
	QString _subPath;
	QString _passwd;
	SyncFlags _flags;
	QSslSocket *_socket;
	QByteArray _inBuf;

private slots:
	void handleDataAvailable();
	void handleSslErrors(const QList<QSslError>& errors);
	void handleDisconnected();
};

Q_DECLARE_OPERATORS_FOR_FLAGS(Agent::SyncFlags)

#endif // AGENT_H
