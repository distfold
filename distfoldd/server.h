#ifndef SERVER_H
#define SERVER_H

#include <QtNetwork/QTcpServer>
#include <QtNetwork/QSslCertificate>
#include <QtNetwork/QSslKey>

class Server : public QTcpServer
{
	Q_OBJECT
public:
	explicit Server(QObject *parent = 0);

protected:
	void loadKeys();
	void incomingConnection(int handle);

private slots:
	void handleSocketError(QAbstractSocket::SocketError error);

private:
	QSslCertificate _cert;
	QSslKey _key;
};

#endif // SERVER_H
