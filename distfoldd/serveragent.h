#ifndef SERVERAGENT_H
#define SERVERAGENT_H

#include "agent.h"

class ServerAgent : public Agent
{
	Q_OBJECT
public:
	explicit ServerAgent(QSslSocket *socket, const QDir& local_dir, const QString& passwd, SyncFlags flags, QObject *parent = 0);

protected:
	void handleMessage(MessageType msg, const QByteArray& data);

private:
	bool checkAuth();
	void handleAuth(const QByteArray& response);
	void handleClientFileList(const RemoteFileInfoList& list);
	void handlePullFile(const QString& path);
	void handlePushedFile(const QByteArray& data);
	void handlePushedMetadata(const RemoteFileInfoList& list);
	void handleDeleteFile(const QString& path);

private:
	QByteArray _challenge;
	QByteArray _clientChallenge;
	bool _authAttempted;
	bool _authOk;
};

#endif // SERVERAGENT_H
