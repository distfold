#ifndef WATCHER_H
#define WATCHER_H

#include <QtCore/QDir>
#include <QtCore/QSocketNotifier>
#include <QtCore/QMap>
#include <QtCore/QHash>

class Watcher : public QObject
{
	Q_OBJECT
public:
	explicit Watcher(const QString& path, QObject *parent = 0);

signals:
	void pathAdded(const QString& path);
	void pathRemoved(const QString& path);
	void pathChanged(const QString& path);
	
private slots:
	void readInotify();

private:
	QStringList scanDirs(const QDir& dir);
	void addWatches(const QStringList &paths);
	void addWatch(const QString& path);
	void removeWatch(const QString& path);
	void removeWatch(int wd);

private:
	int _fd;
	QSocketNotifier *_notifier;
	quint32 _mask;
	QByteArray _buffer;
	QMap<int, QString> _watches;
	QHash<QString, int> _dirs;
};

#endif // WATCHER_H
