#ifndef DISTFOLDER_H
#define DISTFOLDER_H

#include <QtCore/QObject>
#include <QtCore/QDir>
#include <QtCore/QUuid>
#include <QtCore/QFileSystemWatcher>

#include "watcher.h"
#include "server.h"
#include "agent.h"
#include "discoverer.h"

class DistFolder : public QObject
{
	Q_OBJECT
	Q_PROPERTY(QString password READ password WRITE setPassword)
	Q_PROPERTY(bool readOnlySync READ readOnlySync WRITE setReadOnlySync)
	Q_PROPERTY(bool pullMode READ pullMode WRITE setPullMode)
	Q_PROPERTY(bool compress READ compress WRITE setCompress)

public:
	explicit DistFolder(const QUuid& uuid, const QString& path, QObject *parent = 0);

	void setPassword(const QString& passwd);
	QString password() const;
	bool readOnlySync() const;
	void setReadOnlySync(bool read_only);
	bool pullMode() const;
	void setPullMode(bool pull_mode);
	bool compress() const;
	void setCompress(bool compress);

private:
	QDateTime scanLastModTime();
	QDateTime scanLastModTime(const QDir& dir);
	void updateLastModTime(const QDateTime& dt = QDateTime());

private slots:
	void handleNewConnection();
	void handlePathAdded(const QString& path);
	void handlePathChanged(const QString& path);
	void handlePathRemoved(const QString& path);
	void handleMoreRecentHost(const QHostAddress& address, uint port, const QDateTime& dateTime);
	void handleClientFinished();
	void handleDestroyedAgent();

private:
	QUuid _uuid;
	QDir _localPath;
	QDateTime _mtime;
	bool _mtimeChanged;
	Watcher *_watcher;
	Server *_server;
	Discoverer *_discoverer;
	QString _password;
	Agent::SyncFlags _syncFlags;
	int _numAgents;
};

#endif // DISTFOLDER_H
